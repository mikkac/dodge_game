#include <iostream>
#include <chrono>
#include <thread>


const int col=52;
const int row=40;
class gracz{
public:
    int x,y;
    void set_x(int _x);
    void set_y(int _y);
};
class plansza{
public:
    plansza(char x);
    char trasa[row][col];
    void rysuj_plansze(gracz& gr);
    void rysuj();
    void odswiez();
};

void gracz::set_x(int _x){
    x=_x;
}
void gracz::set_y(int _y){
    y=_y;
}

plansza::plansza(char x){
    for(int i=0; i<row; ++i){
        for(int j=0; j<col; ++j){
            trasa[i][j]=x;
        }

    }
}


void plansza::rysuj_plansze(gracz& gr){
    for(int i=0; i<row; ++i){
        trasa[i][0] = '#';
        trasa[i][col-1]= '#';
    }
    gr.set_x(((col-2)/2)-2);
    gr.set_y(row);
    for(int i=0; i<6; ++i){
        for(int j=0; j<4; ++j){
            trasa[gr.y-i][gr.x+j]='#';
        }
    }
    for(int i=0; i<row; ++i){
        for(int j=0; j<col; ++j){
            std::cout << trasa[i][j];
        }
        std::cout << std::endl;
    }
}

void plansza::rysuj(){
    for(int i=0; i<row; ++i){
        for(int j=0; j<col; ++j){
            std::cout << trasa[i][j];
        }
        std::cout << std::endl;
    }
}

void plansza::odswiez(){
    while(1){
        system("clear");
        rysuj();
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
        
    }
}

int main(){
    gracz g1;
    plansza p1{' '};
    p1.rysuj_plansze(g1);
    p1.odswiez();
    return 0;
}